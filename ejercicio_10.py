
class Rectangulo:
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura
    
    def area(self):
        a = self.base * self.altura
        return a

    def perimetro(self):
        p = (self.base * 2) + (self.altura * 2)
        return p
    
rectangulo1 = Rectangulo(8, 5)

print(rectangulo1.area())
