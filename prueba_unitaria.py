import unittest
from ejercicio_10 import Rectangulo
#from ejercicio_10 import perimetro

class TestEj10(unittest.TestCase):
    def test_area(self):
        a = Rectangulo(5,5)
        self.assertEqual(a.area(), 25)

    def test_perimetro(self):
        p = Rectangulo(8,5)
        self.assertEqual(p.perimetro(), 26)

if __name__ == '__main__':
    unittest.main()
